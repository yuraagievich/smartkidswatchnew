$("input[name='user-phone']").mask("+375 (99) 999-99-99");


$('button[type=submit]').on('click', function (e) {
    e.preventDefault();
    var number = $(this).parents('.sendMail').find('input[type=tel]').val();

    if (validatePhone(number) === true) {
        $(this).parents('.sendMail').submit();
        //$(location).attr('href','/mail.php');

    }else {
        console.log($(this));
        console.log('bad');
    }
});

function validatePhone($phone){
    var phoneMask = $phone.split("").slice(6,8);
    var codePhone = phoneMask.join("");

    if(codePhone == "29" || codePhone == "33" || codePhone == "25" || codePhone == "44"){
        return true;
    }

    else{
        return false;
    }
}