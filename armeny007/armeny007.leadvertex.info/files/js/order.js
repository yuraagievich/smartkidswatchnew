$(function() {
var dprice = $('body').attr('data-price');
	var arrName = 
	[
		'Ольга Захарова',
		'Андрей Ровный',
		'Светлана Крапивина',
		'Татьяна Русинова',
		'Алексей Нужонков',
		'Мехрибан Алиева',
		'Гульнара Мамедова',
		'Александра Таранец',
		'Виктория Бережновская',
		'Камила Дивникова',
		'Светлана Тихонова',
		'Елена Старостина',
		'Марина Переверзева',
		'Галина Бондаренко',
		'Валентина Пак',
		'Полина Меркулова'
	]

	var arrCity = 
	[
		'Москва',
		"Санкт-Петербург",
		"Ростов",
		"Челябинск",
		"Екатеринбург",
		'Находка',
		"Санкт-Петербург",
		"Тюмень",
		"Челябинск",
		"Екатеринбург",
		"Ханты-Мансийск",
		"Пермь",
		"Воронеж",
		"Иркутск",
		"Москва",
		"Вологда"
	]


	setInterval( function() {

		if ( window.innerWidth < 750 ) return;

		var nameRand = Math.floor( Math.random()*arrName.length);
		var cityRand = Math.floor( Math.random()*arrCity.length);

		var count = Math.floor( Math.random()*2 );

		if ( count === 0 ) count = 1;


	
		var text = arrName[nameRand] + ', из г. ' + arrCity[cityRand] + ', сделал(а) заказ на сумму ' + dprice*count +
		' руб., количество – ' + count + ' шт.';

		$('.order-text').text(text);
		$('.order').fadeIn( "slow" );


		setTimeout( function() {
			$('.order').fadeOut( "slow" );
		}, 7000);

	}, 30000);

});